package com.mycompany.arquillianjsftest;

import java.io.File;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;

public class Deployments {

    public static WebArchive createWebArchive() throws IllegalArgumentException {
        final ExplodedImporter importer = ShrinkWrap.create(ExplodedImporter.class, "/");
        final ExplodedImporter importDirectory = importer.importDirectory(new File("src/main/webapp"));
        GenericArchive create = importDirectory.as(GenericArchive.class);
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war").setWebXML(new File("src/main/webapp/WEB-INF/web.xml")).addPackage(Package.getPackage("com.mycompany.arquillianjsftest")).merge(create).addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");
        return webArchive;
    }
    
}
