package com.mycompany.arquillianjsftest;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author cbr
 */
@ManagedBean
@SessionScoped
public class NewJSFManagedBean {

    public String welcomePrimefaces() {
        return "welcomePrimefaces?faces-redirect=true";
    }
    
    public String getStringValue() {
        System.err.println("*******************************");
        System.err.println("*******************************");
        System.err.println("*******************************");
        System.err.println("*******************************");
        System.err.println("Getting the string value....");
        return "String Value";
    }
}
