package com.mycompany.arquillianjsftest;

import java.io.IOException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.jsfunit.api.InitialPage;
import org.jboss.jsfunit.jsfsession.JSFClientSession;
import org.jboss.jsfunit.jsfsession.JSFServerSession;
import org.jboss.jsfunit.jsfsession.JSFSession;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class JsfUnitTest {

    @Deployment // testable = true
    public static WebArchive createDeployment() {
        return Deployments.createWebArchive();
    }

    @Test
    @InitialPage("/faces/index.xhtml")
    public void testInitialPage(JSFServerSession server, JSFClientSession client, JSFSession session) throws IOException {
        Assert.assertEquals("/index.xhtml", server.getCurrentViewID());
        Assert.assertEquals("/test/faces/index.xhtml", client.getContentPage().getUrl().getPath());
        System.err.println("contentPage: " + client.getContentPage().getWebResponse().getContentAsString());
        client.click("welcomePrimefaces");
        session.getWebClient().waitForBackgroundJavaScript(10000);
        System.err.println("contentPage: " + client.getContentPage().getWebResponse().getContentAsString());
        Assert.assertEquals("/welcomePrimefaces.xhtml", server.getCurrentViewID());
        Assert.assertEquals("/test/faces/welcomePrimefaces.xhtml", client.getContentPage().getUrl().getPath());
        Object newJSFManagedBean = server.getManagedBeanValue("${newJSFManagedBean}");
        System.err.println("newJSFManagedBean = " + newJSFManagedBean);
    }
}
