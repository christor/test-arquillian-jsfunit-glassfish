package com.mycompany.arquillianjsftest;

import com.thoughtworks.selenium.DefaultSelenium;
import java.io.File;
import java.net.URL;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class DroneTest {

    @Drone
    DefaultSelenium browser;
    @ArquillianResource
    private URL deploymentUrl;

    @Deployment(testable = false) // this needs to be false for the injection of the URL...don't know why...
    public static WebArchive createDeployment() {
        final ExplodedImporter importer = ShrinkWrap.create(ExplodedImporter.class, "/");
        final ExplodedImporter importDirectory = importer.importDirectory(new File("src/main/webapp"));
        GenericArchive create = importDirectory.as(GenericArchive.class);
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Package.getPackage("com.mycompany.arquillianjsftest")) // my test package
                .merge(create).addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");
        return webArchive;
    }

    @Test
    public void testDrone() throws InterruptedException {
        System.err.println("deploymentUrl: " + deploymentUrl);
        browser.open(deploymentUrl.toExternalForm());
        String htmlSource = browser.getHtmlSource();
        System.err.println("htmlSource: " + htmlSource);
        browser.click("id=form:welcomePrimefaces");
        browser.waitForPageToLoad("1000");
        String location = browser.getLocation().substring(browser.getLocation().lastIndexOf("/"));
        Assert.assertEquals("/welcomePrimefaces.xhtml", location);
    }
}
